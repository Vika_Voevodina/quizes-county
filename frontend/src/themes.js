import {darkJoy, darkEva, darkSber} from '@sberdevices/plasma-tokens/themes';
import {logger} from "./logging";
import css from 'json-to-css';

const themes = {eva: darkEva, joy: darkJoy, sber: darkSber}

export function getTheme(character) {
  return css.of(themes[character])
  // document.styleSheets[0].insertRule(c);
}

const sheetIndex = 1

export function setTheme(character) {
  let sheet = document.styleSheets[sheetIndex];

  if (sheet.cssRules[0].selectorText === ":root") {
    sheet.deleteRule(0);
  }
  sheet.insertRule(getTheme(character), 0)
}
