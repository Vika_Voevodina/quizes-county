'use strict';

const {DialogManger} = require("dialute");
const express = require("express");
const chalk = require("chalk");
const fs = require("fs");
const {logger} = require('./logger')

const rawData = fs.readFileSync("puzzles.json")
const puzzles = JSON.parse(rawData);

function choice(choices) {
  const index = Math.floor(Math.random() * choices.length);
  return choices[index];
}


function* script(r) {
  let rsp = r.buildRsp();

  rsp.msg = 'Приветствую в стране загадок! Моя коллекция насчитывает более 2000 вопросов! ' +
    'Все они были предоставлены сайтом riddle-middle.ru. Если у вас возникнут вопросы, скажите Помощь. Начнем игру?';
  rsp.msgJ = 'Привет! Ты попал в страну загадок! В моей коллекции их более 2000 штук. ' +
    'Все они были предоставлены сайтом riddle-middle.ru. Если у тебя возникнет вопрос, скажи Помощь. Начнем игру?';
  yield rsp

  if (r.type === 'MESSAGE_TO_SKILL') {
    if (r.nlu.lemmaIntersection(['нет', 'стоп', 'остановиться', 'остановить', 'хватит', 'выйти', 'выход'])) {
      rsp.msg = 'Всего вам доброго!';
      rsp.msgJ = 'Еще увидимся. Пока!';
      rsp.end = true;
      rsp.data = {'type': 'close_app'};
      yield rsp;
    }
  }
  let puzzle = {};
  let firstTime = true;
  let usedHint = false;
  let triesCount = 0;
  let points = 0;
  while (true) {
    rsp = r.buildRsp();

    function update() {
      puzzle = choice(puzzles);
      rsp.msg = puzzle.question;
      usedHint = false;
      triesCount = 0;

      rsp.data = {
        question: puzzle.question.replace(/\n/g, '<br>'),
        answer: puzzle.answer,
        points
      };
      rsp.listen = true;
    }

    if (r.type === 'SERVER_ACTION') {
      if (r.act.action_id === 'answer') {
        usedHint = true;
        rsp.msg = puzzle.answer;
      } else {
        update();
      }
    } else if (!r.nlu) {
      continue
    } else if (r.nlu.lemmaIntersection(['выход', 'выйти', 'выйди'])) {
      rsp.msg = 'Всего вам доброго!'
      rsp.msgJ = 'Еще увидимся. Пока!'
      rsp.end = true;
      rsp.data = {'type': 'close_app'}
    } else if (r.nlu.lemmaIntersection(['помощь', 'помочь'])) {
      rsp.msg = 'Я буду зачитывать загадки, а вам нужно будет на них отвечать. ' +
        'Каждая успешно отгаданная загадка даст вам один балл. Ответ не будет засчитан, если вы используете подсказку.';
      rsp.msgJ = 'Я буду загадывать загадки, а тебе нужно на них отвечать. ' +
        'Каждая успешно отгаданная загадка даст тебе один балл. Ответ не будет засчитан, если ты используешь подсказку.';

    } else if (firstTime) {
      firstTime = false;
      update();

    } else if (r.msg.toLowerCase() === puzzle.answer) {
      if (!usedHint) {
        points++;
      }
      update();
      rsp.msg = choice(['Правильно!', 'Верно!', 'Молодец!']) + '\n' + puzzle.question;

    } else if (r.nlu.lemmaIntersection(['другой', 'следующий'])) {
      update();

    } else if (r.nlu.lemmaIntersection(['подсказать', 'подсказка', 'ответ', 'ответить'])) {
      usedHint = true;
      rsp.msg = puzzle.answer;

    } else if (triesCount > 3 && r.nlu.lemmaIntersection(['да', 'давай'])) {
      update();

    } else if (triesCount > 3 && r.nlu.lemmaIntersection(['нет'])) {
      triesCount = 0;

    } else {
      triesCount++;
      let text = choice(['Неправильно!', 'Неверно!', 'Не угадали!'])
      if (triesCount > 3) {
        text += choice(['Поменять загадку?', 'Загадать другую?', 'Подумаем над другой загадкой?'])
      }
      rsp.msg = text;
    }
    yield rsp;
  }
}


const dm = new DialogManger(script);
const app = express();
const port = 8000;


app.use((req, res, next) => {
  console.info(`${chalk.cyanBright(new Date().toUTCString())} - ${chalk.green(req.method)}:`, chalk.cyan(req.path));
  next();
});
app.use(express.json());
app.use(express.static('../frontend/public'))
app.use(express.static('public'))
//
app.post('/log', (request, response) => {
  // console.log(request.body);
  response.send('ok')
})

app.post('/app-connector/', (request, response) => {
  const body = dm.process(request.body);
  response.send(body);
});

app.listen(port, () => console.log(chalk.blue(`Start server on http://localhost:${port}/`)));
