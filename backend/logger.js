const pino = require('pino')
const {logflarePinoVercel} = require('pino-logflare')

// create pino-logflare console stream for serverless functions and send function for browser logs
// @ts-ignore
const {stream, send} = logflarePinoVercel({
  apiKey: 'Ln_fScpmXNI0',
  sourceToken: 'e2f14fe7-76f5-42d3-88b7-4dbd1842888f',
})

// create pino loggger
const logger = pino(
  {
    browser: {
      transmit: {
        level: 'info',
        send: send,
      },
    },
    level: 'debug',
    base: {
      env: process.env.ENV || 'ENV not set',
    },
  },
  stream
)

module.exports = {logger}
